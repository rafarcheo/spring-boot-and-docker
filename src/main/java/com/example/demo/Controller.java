package com.example.demo;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value= Controller.ENDPOINT)
@Slf4j
@RestController
@RequiredArgsConstructor
public class Controller {

    static final String ENDPOINT = "/blog";

    private final BlogService blogService;

    @GetMapping
    public Iterable<BasePost> handleGetRequest() {
        log.info("[controller] display all blog post");
        return blogService.list();
    }

    @PutMapping
    public BasePost handlePutRequest(@RequestBody BasePost newPost) {
        log.info("[controller] add new post to database: " + newPost);
        return blogService.addNew(newPost);
    }
}

