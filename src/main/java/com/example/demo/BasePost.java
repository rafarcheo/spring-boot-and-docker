package com.example.demo;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "BASE_BLOG_POST")
@Inheritance(strategy =  InheritanceType.JOINED)
public class BasePost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 'Identify' set to leverage auto increment
    private Long id;

    private String title;

    private String content;

    public BasePost(String title, String content) {
        this.title = title;
        this.content = content;
    }
}
