package com.example.demo;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class BlogService {
    private final PostRepository postRepository;

    public Iterable<BasePost> list() {
        return postRepository.findAll();
    }

    public BasePost addNew(BasePost newPost) {
        return postRepository.save(newPost);
    }
}