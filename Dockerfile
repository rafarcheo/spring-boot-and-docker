FROM openjdk:8-jdk-alpine

COPY target/demo-spring-app*.jar /opt/app/app.jar

EXPOSE 8086

ENTRYPOINT ["java", "-jar", "/opt/app/app.jar"]
