# spring-boot and docker

## Spring boot app - build docker image with gitlab ci

1. Add plugin to main pom.xml

```xml
<build>
	<finalName>[app-name]</finalName>
	<plugins>
		<plugin>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-maven-plugin</artifactId>
		</plugin>
	</plugins>
</build>
```

2.  Create Dockerfile in application main directory

```Dockerfile
FROM openjdk:8-jdk-alpine
COPY target/[app-name]*.jar /opt/app/app.jar
EXPOSE 8086

ENTRYPOINT ["java", "-jar", "/opt/app/app.jar"]
```

3. Use maven to generate jar in target directory 
$ mvn clean install


4. Create local docker image
in dir with Dockerfile run
$ docker build -t  nazwa-image:1.0.0 .
$ docker build -t  lpietrzykowski/shop-image:1.0.0 .

where -t mean tag - name of image 

5. Run local image 
a) eun form cli
$ docker run -p 8080:8080  lpietrzykowski/shop-image:1.0.0
where -p mean port browser-port:container-port

b) run app from docker compose
create docker-compose.yml

```yaml
version: "3"
services:
  web-app:
    image: 'lpietrzykowski/demo:1.0.0'
    ports:
      - "8080:8080"
```

// run docker compose
$ docker-compose -f docker-compose.yml up

7. Add new post to blog

execute PUT request to http://localhost:8086/blog
```json
{
    "title": "new post title",
    "content": "default description 123"
}
```